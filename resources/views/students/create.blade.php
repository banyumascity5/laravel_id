@extends('layout/main')


@section('title', 'Form Tambah Data Mahasiswa')


@section('container')
<div class="container">
        <div class="row">
            <div class="col-7">
                <h1 class="mt-3">Form Tambah Data Mahasiswa</h1>

                <form method="POST" action="/students">
                    @csrf
                   <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control @error('nama') is-invalid
                    @enderror" id="nama" placeholder="masukkan nama" name="nama" 
                    value="{{old('nama') }}">
                    @error ('nama')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                   </div>
                   <div class="form-group">
                    <label for="npm">Npm</label>
                    <input type="text" class="form-control @error('npm') is-invalid
                    @enderror" id="npm" placeholder="masukkan npm" name="npm"
                    value="{{old('npm') }}">
                    @error ('npm')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                   </div>
                   <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control @error('email') is-invalid
                    @enderror" id="email" placeholder="masukkan email" name="email"
                    value="{{old('email') }}">
                    @error ('email')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                   </div>
                   <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <input type="text" class="form-control @error('jurusan') is-invalid
                    @enderror" id="jurusan" placeholder="masukkan jurusan" name="jurusan"
                    value="{{old('jurusan') }}">
                    @error ('jurusan')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                   </div>
                   <button type="summit" class="btn btn-primary my-3">Tambah Data!</button>
                  </form>
            </div>
        </div>
    </div>
@endsection
