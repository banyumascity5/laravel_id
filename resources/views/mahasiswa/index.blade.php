@extends('layout/main')


@section('title', 'Daftar Mahasiswa')


@section('container')
<div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Daftar Mahasiswa</h1>
                
                <table class="table">

                <thead class="table-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Npm</th>
                        <th scope="col">Email</th>
                        <th scope="col">Jurusan</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($mahasiswa as $mahasiswa)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{ $mahasiswa->nama}}</td>
                        <td>{{ $mahasiswa->npm}}</td>
                        <td>{{ $mahasiswa->email}}</td>
                        <td>{{ $mahasiswa->jurusan}}</td>
                        <td>
                            <a href="" class="badge text-success">edit</a>
                            <a href="" class="badge text-danger">delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
